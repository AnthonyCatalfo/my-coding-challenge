function calendarMatching(
  calendar1,
  dailyBounds1,
  calendar2,
  dailyBounds2,
  meetingDuration
) {
  let count = 0;
  const result = [];
  const minutesFromText = (timeText) => {
    const time = timeText.split(":").map((num) => parseInt(num));
    return time[0] * 60 + time[1];
  };

  const timeTextFromMinutes = (n) => {
    const hours = Math.floor(n / 60);
    let minutes = n % 60;

    minutes = minutes < 10 ? `0${minutes}` : `${minutes}`;
    return `${hours}:${minutes}`;
  };

  let arr = new Array(1440).fill(false);
  const timeStart =
    minutesFromText(dailyBounds1[0]) < minutesFromText(dailyBounds2[0])
      ? minutesFromText(dailyBounds2[0])
      : minutesFromText(dailyBounds1[0]);
  const timeEnd =
    minutesFromText(dailyBounds1[1]) < minutesFromText(dailyBounds2[1])
      ? minutesFromText(dailyBounds1[1])
      : minutesFromText(dailyBounds2[1]);

  for (let i = timeStart; i < timeEnd; i++) {
    arr[i] = true;
  }

  let timeUsed = [...calendar1, ...calendar2];
  for (let i = 0; i < timeUsed.length; i++) {
    for (let j = minutesFromText(timeUsed[i][0]); j < minutesFromText(timeUsed[i][1]); j++) {
      if (arr[j]) {
        arr[j] = false;
      }
    }
  }

  for (let i = timeStart; i < timeEnd; i++) {
    if (arr[i]) {
      count++;
    }

    if (count > meetingDuration - 1 && !arr[i]) {
      result.push([timeTextFromMinutes(i - count), timeTextFromMinutes(i)]);
    }

    if (arr[i] && count > meetingDuration - 1 && i === timeEnd - 1) {
      result.push([timeTextFromMinutes(i - count + 1), timeTextFromMinutes(i + 1)]);
    }
    if (!arr[i]) {
      count = 0;
    }
  }
  return result;
}

let calendar1 = [
  ["10:00", "10:30"],
  ["12:00", "13:00"],
  ["16:00", "18:00"],
];

const dailyBounds1 = ["9:00", "20:00"];
const calendar2 = [
  ["10:00", "11:30"],
  ["12:30", "14:30"],
  ["14:30", "15:00"],
  ["16:00", "17:00"],
];
const dailyBounds2 = ["9:00", "18:30"];
const meetingDuration = 30;
calendarMatching(
  calendar1,
  dailyBounds1,
  calendar2,
  dailyBounds2,
  meetingDuration
);
